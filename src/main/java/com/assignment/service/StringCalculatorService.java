package com.assignment.service;

import org.springframework.stereotype.Service;

import com.assignment.exception.NegetiveNumberException;

@Service
public class StringCalculatorService {

	public int add(String str) {

		if (str.trim().isEmpty()) {
			return 0;
		}

		String delemeter = ",";
		String[] delementerArr = null;

		if (str.startsWith("//")) {
			delemeter = str.substring(2, str.indexOf("\n"));
			if (delemeter.trim().startsWith("[") && delemeter.trim().endsWith("]")) {
				delemeter = delemeter.trim().substring(1, delemeter.length() - 1);
				if (delemeter.contains("][")) {
					delemeter = delemeter.replace("][", "]");
					delementerArr = delemeter.split("]");
					delemeter = delementerArr[0];
				}
			}
			str = str.substring(str.indexOf("\n") + 1);
		}

		if (delementerArr != null) {
			for (String delementerLocal : delementerArr) {
				if (!delementerLocal.equals(delemeter)) {
					str = str.replace(delementerLocal, delemeter);
				}
			}
		}

		str = str.replace("\n", delemeter);
		String[] strArr = str.trim().split(delemeter);

		String negetiveNumbers = "";
		boolean isNegetivePresent = false;

		int sum = 0;
		for (String local : strArr) {
			int n = Integer.parseInt(local);
			if (n < 0) {
				isNegetivePresent = true;
				negetiveNumbers += n + " ";
				continue;
			} else if (n > 1000) {
				continue;
			}
			sum += n;
		}

		if (isNegetivePresent) {
			throw new NegetiveNumberException("negatives not allowed " + negetiveNumbers.trim());
		}
		return sum;

	}
}
