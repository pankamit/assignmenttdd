package com.assignment;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.assignment.exception.NegetiveNumberException;
import com.assignment.service.StringCalculatorService;

@SpringBootTest
class AssignmentTddApplicationTests {

	@Autowired
	private StringCalculatorService stringCalculatorService;

	@Test
	public void addWhenEmptyInputStringThenSuccess() {
		int actualResult = stringCalculatorService.add("");
		int expectedResult = 0;
		assertEquals(expectedResult, actualResult);
	}

	@Test
	public void addWhen1InputParameterThenSuccess() {
		int actualResult = stringCalculatorService.add("2");
		int expectedResult = 2;
		assertEquals(expectedResult, actualResult);
	}

	@Test
	public void addWhen2InputParameterThenSuccess() {
		int actualResult = stringCalculatorService.add("1,2");
		int expectedResult = 3;
		assertEquals(expectedResult, actualResult);
	}

	@Test
	public void addWhenUnknownInputParameterThenSuccess() {
		int actualResult = stringCalculatorService.add("1,2,3,4");
		int expectedResult = 10;
		assertEquals(expectedResult, actualResult);
	}

	@Test
	public void addWhenNewLinesBetweenInputParametersThenSuccess() {
		int actualResult = stringCalculatorService.add("1\n2,3");
		int expectedResult = 6;
		assertEquals(expectedResult, actualResult);
	}

	// “//[delimiter]\n[numbers…]”
	@Test
	public void addWhenHandleDifferentDelemeterBetweenInputParametersThenSuccess() {
		int actualResult = stringCalculatorService.add("//;\n1;2");
		int expectedResult = 3;
		assertEquals(expectedResult, actualResult);
	}

	@Test
	public void addWhenNegetiveNumberExcpetionThenSuccess() {

		Exception exception = assertThrows(NegetiveNumberException.class,
				() -> stringCalculatorService.add("//;\n1;-2;3"));

		String expectedMessage = "negatives not allowed -2";
		assertEquals(expectedMessage, exception.getMessage());

	}

	@Test
	public void addWhenMultipleNegetiveNumberExcpetionThenSuccess() {

		Exception exception = assertThrows(NegetiveNumberException.class,
				() -> stringCalculatorService.add("//;\n1;-2;3;-4;5"));

		String expectedMessage = "negatives not allowed -2 -4";
		assertEquals(expectedMessage, exception.getMessage());

	}

	@Test
	public void addWhenInputBigger1000IgnoredThenSuccess() {
		int actualResult = stringCalculatorService.add("2,1001");
		int expectedResult = 2;
		assertEquals(expectedResult, actualResult);
	}

	@Test
	public void addWhenInputLessOrEqual1000ConsiderThenSuccess() {
		int actualResult = stringCalculatorService.add("2,1000");
		int expectedResult = 1002;
		assertEquals(expectedResult, actualResult);
	}

	// “//[delimiter]\n[numbers…]”
	@Test
	public void addWhenHandleDelemeterDiifentLengthThenSuccess() {
		int actualResult = stringCalculatorService.add("//[&&&]\n1&&&2&&&3");
		int expectedResult = 6;
		assertEquals(expectedResult, actualResult);
	}

	// “//[delim1][delim2]\n”
	@Test
	public void addWhenHandleMultipleDelemeterThenSuccess() {
		int actualResult = stringCalculatorService.add("//[&][%][;]\n1&2%3%4;5");
		int expectedResult = 15;
		assertEquals(expectedResult, actualResult);
	}

	// //[delim1][delim2]\n”
	@Test
	public void addWhenHandleMultipleDelemeterMoreThen1LengthThenSuccess() {
		int actualResult = stringCalculatorService.add("//[&&][%%]\n1&&2%%3");
		int expectedResult = 6;
		assertEquals(expectedResult, actualResult);
	}
}
